from app.game import Game
import json
import urlparse
import os
import sys
import subprocess
import signal
import BaseHTTPServer

HOST_NAME = 'localhost'
PORT_NUMBER = 19234


class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    games = {}
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.send_header("Access-Control-Allow-Origin", "*")
        s.end_headers()
        if s.path == '/':
            s.wfile.write("<html><head><title>Title goes here.</title></head>")
            s.wfile.write("<body><p>This is a test.</p>")
            s.wfile.write("</body></html>")
        else:
            res = urlparse.urlparse(s.path);
            q = res.query
            print q
            msg = urlparse.parse_qs(q)
            for k in msg:
                if k != 'names':
                    msg[k] = msg[k][0]
            if msg['token'] not in MyHandler.games:
                MyHandler.games[msg['token']] = Game()
            if 'actions' in msg:
                actions = json.loads(msg['actions'])
                print actions

                response = str(MyHandler.games[msg['token']].receive(actions)).replace("'", '"')
                # print "[RESPONSE]", response
                s.wfile.write(response)
            else:
                response = str(MyHandler.games[msg['token']].receive(msg)).replace("'", '"')
                # print response
                s.wfile.write(response)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print 'expecting parameter: start | stop'
    elif len(sys.argv) == 2:
        if sys.argv[1] == 'start':
            server_class = BaseHTTPServer.HTTPServer
            httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
            httpd.serve_forever()
            httpd.server_close()
        elif sys.argv[1] == 'stop':
            p = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
            out, err = p.communicate()
            for line in out.splitlines():
                if 'prometheus_strategic_bullying_server.py start' in line:
                    pid = int(line.split(None, 1)[0])
                    os.kill(pid, signal.SIGKILL)
        else:
            print 'unrecognized parameter:', sys.argv[1], 'start | stop expected'
    else:
        print 'too many parameters'
