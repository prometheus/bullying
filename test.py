
from app.game import Game
import random
from time import gmtime, strftime
import json
ts = 0

import pprint as pp 


'''
init a manual game with 
server messages 
'''

serverMsg = [
# Bob = 0
# Jasmine = 1
# Dennis = 2
# Obama =3 
{"start":"True","players":"4","names":["Bob","Jasmine","Dennis","Obama"]},

{"action":"skip","player":"0"},
{"action":"skip","player":"1"},
{"action":"skip","player":"2"},

# Obama(3) creates coalition USA(0)
{"action":"start","player":"3","candidate":"None"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},


# Dennis(2) creates coalition HPS(1)
{"action":"start","player":"2"},
{"action":"skip","player":"3"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},

# Dennis(2) invites Bob(0) and Jasmine(1)
# Obama(3) invites Jasmine(1)
{"action":"invite","player":"2","candidate":"0"},
{"action":"skip","player":"3"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},

{"action":"invite","player":"2","candidate":"1"},
{"action":"invite","player":"3","candidate":"1"},

# Bob(0) accepts HPS(1)
# Jasmine(1) declines USA(0), accepts HPS(1)
{"action":"accept","player":"0","coalition":"1"},
{"action":"refuse","player":"1","coalition":"0"},

{"action":"skip","player":"2"},
{"action":"skip","player":"3"},
{"action":"skip","player":"0"},

{"action":"accept","player":"1","coalition":"1"},
{"action":"skip","player":"2"},


# Obama(3) challenges HPS(1)
{"action":"challenge","player":"3","coalition":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},
{"action":"skip","player":"2"},

# Obama(3) disengages with HPS(1)
{"action":"disengage","player":"3","coalition":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},
{"action":"skip","player":"2"},

# Obama(3) rechallenges HPS(1)
{"action":"challenge","player":"3","coalition":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},
{"action":"skip","player":"2"},

# Obama(3) goes to war with HPS(1) 
{"action":"engage","player":"3","coalition":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},


# Dennis(2) ejects Jasmine(1) 
{"action":"eject","player":"2","candidate":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},

# Dennis(2) invites Jasmine(1) 
{"action":"invite","player":"2","candidate":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},

# Dennis(2) uninvites Jasmine(1) 
{"action":"uninvite","player":"2","candidate":"1"},
{"action":"skip","player":"0"},

# Jasmine(1) creates coalition
{"action":"start","player":"1"},

# Dennis(2) challenges Jasmine(1)
{"action":"challenge","player":"2", "coalition":"2"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},

# Dennis(2) disengages Jasmine(1)
{"action":"disengage","player":"2", "coalition":"2"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},

# Jasmine(1) challenges Dennis(2)
{"action":"challenge","player":"1", "coalition":"1"},
{"action":"skip","player":"0"},
{"action":"skip","player":"1"},
{"action":"skip","player":"2"},

# Jasmine(1) engage Dennis(2)
{"action":"engage","player":"1", "coalition":"1"},

# Both players skip 2 rounds
{"action":"skip","player":"2"},
{"action":"skip","player":"0"},
{"action":"skip","player":"2"},
{"action":"skip","player":"0"},


]

app = Game()
# app.printGame()

print "\nSTART GAME\n"
while ts < len(serverMsg):
	print "---TIME", ts
	# print json.dumps(serverMsg[ts])
	status = app.receive(serverMsg[ts])
	print "[GAME]:"
	pp.pprint(status)
	# app.receive(serverMsg[ts])

	ts += 1



