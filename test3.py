
from app.game import Game
import random
from time import gmtime, strftime
import json
import pprint as pp 


# Bob = 0
# Ai1 = 1b
# Ai2 = 2
app = Game()

def send(msg, ts = 0):
    print "---TIME", ts
    status = app.receive(msg)
    print "[GAME]:"
    pp.pprint(status)
    ts += 1 
print "\nSTART GAME\n"

msg1 = {"start":"True","players":"3","names":["Bob"]}
msg2 = [{"action":"start","player":"0"},{"action":"invite","player":"0","candidate":"1"}]
msg3 = {"action":"skip","player":"0"}
msg4 = {"action":"skip","player":"0"}


send(msg1)
send(msg2)
for i in xrange(10):
    send(msg3)


print "\n[FINAL UPDATE]"
pp.pprint(app.printGame())


