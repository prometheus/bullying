from coalition import Coalition
import json
class War():

	def __init__(self, challenger, challengee):
		self.challenger = challenger
		self.challengee = challengee
		self.victor = None
		self.loser = None

	def battle(self):

		if self.challenger.power == self.challengee.power:
			return False

		if self.challenger.power > self.challengee.power:
			self.victor = self.challenger
			self.loser = self.challengee
		else:
			self.victor = self.challengee
			self.loser = self.challenger
		return True


	def distributeWealth(self):

		for agent in self.victor.agents:
			distribution = (agent.power/self.victor.power)
			agent.update(agent.wealth + round(distribution * self.loser.wealth, 2))

		self.victor.update()

	def __repr__(self):
		return json.dumps({"Challenger":str(self.challenger.id),"Challengee":str(self.challengee.id),"Victor":str(self.victor.id), "Loser":str(self.loser.id)})

		# return json.dumps({"Challenger":str(self.challenger.id),"Challengee":str(self.challengee.id),"Victor":str(self.victor.id), "Loser":str(self.loser.id)}),
