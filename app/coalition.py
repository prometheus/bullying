from agent import Agent
import json


class Coalition():
	def __init__(self,num,leader):
		self.id = num
		self.leader = leader
		self.agents = [leader]
		self.wealth = leader.wealth
		self.power = leader.power
		self.received_invitations = []
		self.sent_invitations = []

	def addAgent(self,agent):
		if agent in self.agents:
			return False

		self.wealth += agent.wealth
		self.power += agent.power
		# self.update()
		self.agents.append(agent)

		return True

	def removeAgent(self,agent):
		if agent not in self.agents:
			return False

		self.wealth -= agent.wealth
		self.power -= agent.power

		# self.update()
		self.agents.remove(agent)

		return True

	def update(self):
		self.wealth = sum([a.wealth for a in self.agents])
		self.power = sum([a.power for a in self.agents])


	def __repr__(self):
		return json.dumps({"Id":self.id, "Status":{"Power":self.power,"Wealth":self.wealth, "Leader":self.leader.id, "Size":len(self.agents), "receivedInvitations":[str(i) for i in self.received_invitations], "sentInvitations":[str(i) for i in self.sent_invitations], "Members":[str(agent.id) for agent in self.agents]}})


