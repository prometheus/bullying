from coalition import Coalition

class Action():

	ACCEPT = "accept"
	START = "start"
	REFLECT = "reflect"
	INVITE = "invite"
	EJECT = "eject"
	REFUSE = "refuse"
	UNINVITE = "uninvite"
	CHALLENGE = "challenge"
	ENGAGE = "engage"
	DISENGAGE = "disengage"
	SKIP = "skip"


	'''
	candidate accepts invitation into a coalition
	returns false if coalition has not invited candidate
	by checking candidate.invitation's list
	'''
	def accept(self, candidate, leader):
		if candidate.id not in leader.sent_invitations:
			return False

		if not leader.coalition:
			return False	

		if candidate.coalition:
			return False	

		coalition = leader.coalition
		coalition.addAgent(candidate)
		candidate.join(coalition)

		leader.sent_invitations.remove(candidate.id)
		candidate.received_invitations[:] = []
		return True

	'''
	start a coalition by creating a new 
	coalition with agent as leader
	returns false if agent already in coalition
	'''
	def start(self, agent, coalitionId):
		if agent.coalition is not None:
			return None
		
		coalition = Coalition(coalitionId, agent)
		agent.join(coalition)
		agent.isLeader = True

		return coalition

	'''
	quit a coalition if you are a member
	'''
	def reflect(self, candidate):
		if candidate.coalition is None:
			return False
		if candidate.isLeader:
			return False
		
		candidate.coalition.removeAgent(candidate)
		candidate.quit()

		return True

	'''
	agent invite an candidate into a coalition by adding
	the coalition id to list received_invitations within
	candidate class
	'''
	def invite(self, leader, candidate, coalition):
		if candidate.id not in leader.sent_invitations:
			candidate.received_invitations.append(leader.id)
			leader.sent_invitations.append(candidate.id)
		return True

	'''
	agent eject candidate from coalition
	returns false if candidate not in coalition
	or if agent is not leader of coalition
	'''
	def eject(self, agent, candidate, coalition):
		if candidate.coalition != coalition or candidate not in coalition.agents:
			return False

		candidate.quit()
		coalition.agents.remove(candidate)

		return True

	'''
	candidate refuses a coalition invitation
	if candidate does not have coalition invitation, 
	return false
	'''
	def refuse(self,member, leader):
		if member.id not in leader.sent_invitations:
			return False

		coalition = leader.coalition

		member.received_invitations.remove(leader.id)
		leader.sent_invitations.remove(member.id)

		return True

	'''
	agent remove existing invitation to candidate
	for agent's coalition. returns false if agent
	is not leader of coalition or invitation does
	not exist
	'''
	def uninvite(self, agent, candidate, coalition):
		if agent != coalition.leader:
			return False

		if candidate.id not in coalition.leader.sent_invitations:
			return False

		candidate.received_invitations.remove(agent.id)
		agent.sent_invitations.remove(candidate.id)

		return True

	'''
	the challenger coalition (leader = agent) invites
	the challengee coalition to war; the challenger 
	coalition id is appended to the challengee's invitation
	list until engaged
	'''
	def challenge(self, agent, challenger, challengee):
		if agent != challenger.leader:
			return False

		if challenger not in challengee.received_invitations:
			challengee.received_invitations.append(challenger.id)
			challenger.sent_invitations.append(challengee.id)

		return True

	'''
	the challenger coalition (leader = agent) engages
	the challenger coalition to war; the challenger id 
	is removed from invitation list and war starts
	'''
	def engage(self, agent, challenger, challengee):
		if agent != challenger.leader:
			return False

		if challenger.id not in challengee.received_invitations:
			return False

		challengee.received_invitations.remove(challenger.id)
		challenger.sent_invitations.remove(challengee.id)


		return True

	'''
	the challengee coalition (leader = agent) engages
	the challenger coalition to war; the challenger id 
	is removed from invitation list and war starts
	'''
	def disengage(self, agent, challenger, challengee):
		if agent != challenger.leader:
			return False

		if challenger.id not in challengee.received_invitations:
			return False
		challengee.received_invitations.remove(challenger.id)
		challenger.sent_invitations.remove(challengee.id)
		return True

