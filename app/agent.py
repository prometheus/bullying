# from coalition import Coalition
import json

class Agent(object):
	def __init__(self, playerId, playerName):
		self.id = playerId
		self.name = playerName
		self.power = 0
		self.wealth = 0
		self.coalition = None
		self.coalitionId = "None"
		self.received_invitations = []
		self.sent_invitations = []
		self.isLeader = False

	def join(self, coalition):
		self.coalition = coalition
		self.coalitionId = coalition.id

	def quit(self):
		self.coalition = None
		self.coalitionId = "None"

	def update(self, wealth):
		self.wealth = wealth
		self.power = wealth * 5

	def __repr__(self):
		# return "Agent \"%s\": Power = %d Wealth = %d Coalition = %s" %(self.name, self.power, self.wealth, coalitionId)
		return json.dumps({"Id":self.id,"Name":self.name, "Status":{"Power":self.power, "Wealth":self.wealth, "Coalition":self.coalitionId, "receivedInvitations":[str(i) for i in self.received_invitations], "sentInvitations": [str(i) for i in self.sent_invitations]}})


