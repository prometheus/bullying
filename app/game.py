
import random
import time
import pprint

from agent import Agent
from ai import AI
from coalition import Coalition
from action import Action
from war import War

import sys
import json

pp = pprint.PrettyPrinter(indent=4)

class Game(object):

    def __init__(self):
        self.agents = {}
        self.coalitions = {}
        self.activeAgents = []
        self.allegianceMap = {}
        self.wars = "None"
        self.numPlayers = 0
        self.activeAI = []
        self.hasAi = False
        self.timestep = 0
        self.gameover = False
        self.action = Action()
        self.coalition_id_tracker = 0
        self.server_msg = None
        self.errors = []
        self.ai_actions = []
        self.maxTimesteps = 0
        self.log = []
        self.playerTurn = 0
        self.initialized = False
        self.skipCount = 0

        self.test = ""
        self.server=""

    '''
    start the game by:
        get number of players from server
        initialize game based on number of players 
            i.e. distribute wealth and power
        print the game status

    loop while not gameover:
        sleep until server response
        parse message and commit action
        print game status
    '''
    def start(self, numPlayers, names):

        # while not self.server_msg:
        #     time.sleep(1)
        #     print "Sleep"

        self.initPlayers(numPlayers, names)
        self.initState()
        # self.printGame()

        # while not self.gameover:
        #     # server_msg = None
        #     playerTurn = self.timestep % self.numPlayers

        #     print "[GAME] TS = %d; Turn = Player %d" %(self.timestep, playerTurn)
        #     print "Player %s: %s" %(self.agents[playerTurn].name, server_msg)

        #     self.parse(server_msg)
        #     self.printGame()



    '''
    Update game status by removing the losers
    in the losing coalition after the end of the
    war

    check for stability and update if game over
    '''
    def update(self, coalition):
        for agent in coalition.agents:
            #clean agent from other agents
            for otherId in self.agents.keys():
                otherAgent = self.agents[otherId]
                if otherAgent != agent:
                    if int(agent.id) in otherAgent.sent_invitations:
                        otherAgent.sent_invitations.remove(int(agent.id))
                    if int(agent.id) in otherAgent.received_invitations:
                        otherAgent.received_invitations.remove(int(agent.id))

            # clean coalition frm other coalitions
            for otherId in self.coalitions.keys():
                otherCoalition = self.coalitions[otherId]
                if otherCoalition != coalition:
                    if int(coalition.id) in otherCoalition.sent_invitations:
                        otherCoalition.sent_invitations.remove(int(coalition.id))
                    if int(coalition.id) in otherCoalition.received_invitations:
                        otherCoalition.received_invitations.remove(int(coalition.id))
           

            for agent in coalition.agents:
                self.agents.pop(str(agent.id), None)
                self.activeAgents.remove(int(agent.id))

                if int(agent.id) in self.activeAI:
                    self.activeAI.remove(int(agent.id))

            self.numPlayers -= 1


        self.coalitions.pop(str(coalition.id), None)

        if not self.stable():
            self.gameover = True

    '''
    determine stability of war by checking
    if current timestep is greater than
    max timestep; also ends game if only
    one player left
    '''
    def stable(self):
        if self.timestep > self.maxTimesteps:
            self.log.extend(["Gameover! Exceeded maximum timesteps!"])
            return False

        if len(self.agents.keys()) == 1:
            self.log.extend(["Gameover! Winner!"])
            return False

        if self.skipCount > (len(self.activeAgents)*2) -1:
            self.log.extend(["Gameover! No one moved for two full rounds!"])
            return False

        return True

    def receive(self,message):

        print "[SERVER TS %d]: %s" %(self.timestep,message)
        self.log[:] = []

        ## only for initial message
        if not self.initialized:
            if "start" in message.keys():
                self.initPlayers(int(message["players"]), message["names"])
                self.initState()
                self.initialized = True

        else: 
            actionSuccess = False
            if isinstance(message, list):
                for msg in message:
                    if self.parse(msg):
                        actionSuccess = True
            else:
                actionSuccess = self.parse(message)

            if actionSuccess:
            # logs.append(self.printGame())
                self.timestep += 1
                self.playerTurn +=1
                # print int(self.playerTurn) not in self.activeAgents
                while self.playerTurn not in self.activeAgents:
                    self.playerTurn +=1
                    if self.playerTurn >= self.numPlayers:
                        self.playerTurn = 0

                ## runs AI moves after getting player turn
                ## if game has ai
                if self.hasAi:
                    actions = []
                    while self.playerTurn in self.activeAI:
                        if self.playerTurn in self.activeAI:
                            self.moveAi(self.playerTurn)
                            self.timestep += 1
                        self.playerTurn +=1

                    if self.playerTurn >= self.numPlayers:
                        self.playerTurn = 0

        while self.playerTurn not in self.activeAgents:
            self.playerTurn +=1
            if self.playerTurn >= self.numPlayers:
                self.playerTurn = 0

        # return logs
        print "[GAME TS %d]: %s" %(self.timestep,self.printGame())

        return self.printGame()
        # return self.prettyPrintGame()



    def moveAi(self, playerTurn):

        player = self.agents[str(playerTurn)]

        player.updateWorld(self.agents, self.coalitions)
        decision = player.generateMove()

        decision_json = json.loads(decision)
        self.parse(decision_json)
        # if not self.parse(decision_json):
        #     self.log.extend(["AI made invalid decision"
        # else:
            # self.timestep += 1
            # while int(self.playerTurn) not in self.activeAgents:
            #     self.playerTurn +=1
            #     if self.playerTurn >= self.numPlayers:
            #         self.playerTurn = 0

        # return json.dumps({"player":player.name, "decision":decision_json})
        return player.status

    '''
    parse server message
    all actions require key "player" and "coalition"
    some actions require key "candidate"

    if keys are missing, do nothing, else
    commit action and update game
    '''
    def parse(self, message):
        player = None
        coalition = None
        candidate = None
        actionSuccess = False
        ## player info required
        # if not player:
        #     self.errors.append(["Player key missing"])
        #     return False

        # ## coalition info required
        # if not coalition:
        #     self.errors.append(["Coalition key missing"])
        #     return False

        # ## following actions require candidate info
        # if not candidate:
        #     self.errors.append(["Candidate key missing"])
        #     return False

        self.wars = "None"

        ## parse message for info
        if "player" in message.keys() and message['player'] != "None" and str(message['player']) in self.agents:
            player = self.agents[str(message["player"])]


        if message["action"] == self.action.SKIP:
            if not player:
                self.log.extend(["player ID does not exist!"])
                return False

            self.log.extend(["%s (ID%s) skipped their turn" %(player.name, str(player.id))])
            self.skipCount += 1
            if not self.stable():
                self.gameover = True

            return True

        if "coalition" in message.keys() and message['coalition'] != "None" and str(message['coalition']) in self.coalitions:
            coalition = self.coalitions[str(message["coalition"])]

        if "candidate" in message.keys() and message['candidate'] != "None" and str(message['candidate']) in self.agents:
            candidate = self.agents[str(message["candidate"])]

        self.skipCount = 0

        ## adds new coalition to list and reutrn success or not
        if message["action"] == self.action.START:
            if not player:
                self.log.extend(["player ID does not exist!"])
                return False

            new_coalition = self.action.start(player, self.coalition_id_tracker)

            if new_coalition is None:
                self.log.extend(["%s (ID%s) was not able to create a coalition!" %(player.name, str(player.id))])
                return False

            self.coalitions.update({str(self.coalition_id_tracker):new_coalition})
            self.coalition_id_tracker += 1

            self.log.extend(["%s (ID%s) created a new coalition!" %(player.name, str(player.id))])
            return True

        ## returns whether accept was successful or not
        if message["action"] == self.action.ACCEPT:
            if not player or not candidate:
                self.log.extend(["player ID or candidate ID does not exist"])
                return False

            if not candidate.coalition:
                self.log.extend(["candidate does not have a coalition"])
                return False

            coalition = candidate.coalition

            if self.action.accept(player, candidate):
                self.log.extend(["%s (ID%s) joined %s (ID%s)s coalition (ID%s) " %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to join %s (ID%s)s coalition (ID%s) " %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return False


        ## returns if invitation successfully refused
        if message["action"] == self.action.REFUSE:
            if not player or not candidate:
                self.log.extend(["player ID or candidate ID does not exist"])
                return False

            if not candidate.coalition:
                self.log.extend(["candidate does not have a coalition"])
                return False

            coalition = candidate.coalition

            if self.action.refuse(player, candidate):
                self.log.extend(["%s (ID%s) declined %s (ID%s)s coalition (ID%s) " %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to decline %s (ID%s)s coalition (ID%s) " %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])


        ## returns if candidiate succesfully quits coalition
        if message["action"] == self.action.REFLECT:
            if not player:
                self.log.extend(["player ID does not exist"])
                return False
            coalition = player.coalition
            if self.action.reflect(player):
                self.log.extend(["%s (ID%s) quit %s (ID%s)s coalition (ID%s) " %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to quit their coalition (ID%s) " %(player.name, str(player.id), str(player.coalition.id))])
                return False


        ## returns if war invitation  successfully made
        if message["action"] == self.action.CHALLENGE:
            if not player or not coalition:
                self.log.extend(["player ID or coalition ID does not exist"])
                return False

            if not player.coalition:
                self.log.extend(["%s (ID%s) is not in a coalition!" %(player.name, str(player.id))])
                return False

            if player.coalition == coalition:
                self.log.extend(["%s (ID%s) can not challenge their own coalition!!" %(player.name, str(player.id))])
                return False

            if self.action.challenge(player, player.coalition, coalition):
                self.log.extend(["%s (ID%s) sent a challenge to %s (ID%s)s coalition (ID%s) " %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])

                return True
            else:
                self.log.extend(["%s (ID%s) was unable to send a challenge to %s (ID%s)s coalition (ID%s)" %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return False


        ## returns if war successfully finished
        if message["action"] == self.action.ENGAGE:
            if not player or not coalition:
                self.log.extend(["player ID or coalition ID does not exist"])
                return False

            init_war = self.action.engage(player,player.coalition,coalition)
            war = None

            if init_war:
                war = War(player.coalition, coalition)
                
                # returns false if war is tie
                if war.battle():
                    war.distributeWealth()

                self.update(war.loser)

                self.wars = war

                victor = war.victor
                loser = war.loser

                self.log.extend(["WAR!!! %s (ID%s)s coalition (ID%s) defeated %s (ID%s)s coalition (ID%s)" %(victor.leader.name, str(victor.leader.id), str(victor.id),loser.leader.name,str(loser.leader.id), str(loser.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to engage in war with %s (ID%s)s coalition (ID%s)" %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return False


        ## returns if war invitation successfully declined
        if message["action"] == self.action.DISENGAGE:
            if not player or not coalition:
                self.log.extend(["player ID or coalition ID does not exist"])
                return False

            if self.action.disengage(player, player.coalition,coalition):
                self.log.extend(["%s (ID%s) disenged in war with %s (ID%s)s coalition (ID%s)" %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to disengage in war with %s (ID%s)s coalition (ID%s)" %(player.name, str(player.id), coalition.leader.name, str(coalition.leader.id), str(coalition.id))])
                return False

        ## returns if invitaiton successfully made to candidate
        if message["action"] == self.action.INVITE:
            if not player:
                self.log.extend(["player ID does not exist!"])
                return False

            if not candidate:
                self.log.extend(["player ID does not exist!"])
                return False

            if not player.coalition or not player.isLeader:
                self.log.extend(["%s (ID%s) is not a leader of a coalition!" %(player.name, str(message["player"]))])
                return False

            if  candidate.coalition:
                self.log.extend(["%s (ID%s) is already in a coalition!" %(candidate.name, str(message["candidate"]))])
                return False

            if self.action.invite(player, candidate, player.coalition):
                self.log.extend(["%s (ID%s) invited %s (ID%s) to their coalition (ID%s)" %(player.name, str(player.id), candidate.name, str(candidate.id), str(player.coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to invite %s (ID%s) to their coalition (ID%s)" %(player.name, str(player.id), candidate.name, str(candidate.id), str(coalition.id))])
                return False

        ## returns if candidate successfully ejected from coalition
        if message["action"] == self.action.EJECT:
            if not player:
                self.log.extend(["player ID does not exist"])
                return False

            if not candidate:
                self.log.extend(["Invalid player ID%s" %(str(candidate.id))])
                return False

            if not player.coalition or not player.isLeader:
                self.log.extend(["player ID%s is not a leader of a coalition!" %(str(message["player"]))])
                return False

            if  not candidate.coalition:
                self.log.extend(["player ID%s is not in a coalition!" %(str(message["candidate"]))])
                return False

            if  candidate.coalition != player.coalition:
                self.log.extend(["player ID%s is not in the coalition!" %(str(message["candidate"]))])
                return False

            if self.action.eject(player, candidate, player.coalition):
                self.log.extend(["%s (ID%s) ejected %s (ID%s) from their coalition (ID%s)" %(player.name, str(player.id), candidate.name, str(candidate.id), str(player.coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to eject %s (ID%s) from their coalition (ID%s)" %(player.name, str(player.id), candidate.name, str(candidate.id), str(player.coalition.id))])
                return False

        ## returns if candidate successfully uninviated from coalition
        if message["action"] == self.action.UNINVITE:
            if not player:
                self.log.extend(["player ID does not exist"])
                return False

            if not candidate:
                self.log.extend(["Candidate ID does not exist"])
                return False

            if not player.coalition or not player.isLeader:
                self.log.extend(["player ID%s is not a leader of a coalition!" %(str(message["player"]))])
                return False

            if self.action.uninvite(player, candidate, player.coalition):
                self.log.extend(["%s (ID%s) uninvited %s (ID%s) to their coalition (ID%s)" %(player.name, str(player.id), candidate.name, str(candidate.id), str(player.coalition.id))])
                return True
            else:
                self.log.extend(["%s (ID%s) was unable to uninvit %s (ID%s) to their coalition (ID%s)" %(player.name, str(player.id), candidate.name, str(candidate.id), str(player.coalition.id))])
                return False
        return False

    def initPlayers(self, num, names):
        # print "Enter number of players: "
        # self.numPlayers = int(self.getFromJS())
        self.numPlayers = num

        if self.numPlayers == 0 or self.numPlayers > 8:
            return False

        for playerId, name in enumerate(names):
            # print "Enter player %d name: " % playerId
            # playerName = self.getFromJS()

            agent = Agent(str(playerId), name)
            self.agents.update({str(playerId):agent})
            self.activeAgents.append(int(agent.id))

        for playerId in xrange(self.numPlayers - len(names)):
            aiId = playerId + len(names)
            ai = AI(aiId)
            self.agents.update({str(aiId):ai})
            self.activeAI.append(int(aiId))
            self.activeAgents.append(int(ai.id))


        if len(self.activeAI) > 0:
            self.hasAi = True




    '''
    initialize the state of the game
    init computations:
        wealth = a rand percentage b/n [80, 100] of (numPlayers * 100)
        power = 5 * wealth
        distribution = normalvariate distribution for mean 100 and std 50
            each player receives their distribution * wealth
    '''
    def initState(self):
        initWealth = self.numPlayers*100
        initPowerRatio = 5
        initShare = [float(random.randrange(75,125)) for x in xrange(self.numPlayers)]
        

        share = [s/sum(initShare) for s in initShare]
        totWealth = random.randint(.8*initWealth, 1.2 *initWealth)

        for i, agent in enumerate(self.agents.values()):
            agent.wealth = round(share[i] * totWealth,2)
            agent.power = round(agent.wealth * initPowerRatio,2)

        self.maxTimesteps = 35 * self.numPlayers

    '''
    print the game status
    return this to server
    '''
    def printGame(self):

        agent_json = []
        coalitions_json = [] 

        for key in sorted(self.agents.iterkeys()):
            agent_json.append(self.agents[key])

        for key in sorted(self.coalitions.iterkeys()):
            coalitions_json.append(self.coalitions[key])

        log_str = "\n".join(self.log)

        if self.hasAi:
            status = {"clock":self.timestep, "playerTurn":self.playerTurn, "currentPlayers":self.activeAgents, "currentCoalitions":self.coalitions.keys(), "agents":agent_json, "coalitions":coalitions_json, "wars":self.wars, "gameover":str(self.gameover), "log":log_str}
        else:
            status = {"clock":self.timestep, "playerTurn":self.playerTurn, "currentPlayers":self.activeAgents, "currentCoalitions":self.coalitions.keys(), "agents":agent_json, "coalitions":coalitions_json, "wars":self.wars, "gameover":str(self.gameover), "log":log_str}
            # status = {"clock":self.timestep, "playerTurn":playerTurn, "currentPlayers":self.agents.keys(),"currentCoalitions":self.coalitions.keys(),"agents":self.agents, "coalitions":self.coalitions, "wars":self.wars, "gameover":self.gameover, "errors":self.errors}
        return status

    '''
    pretty print the game status

    return this to server
    '''
    def prettyPrintGame(self):
        def toJson(msg):
            return msg[1:-1]
        status = {"clock":self.timestep, "agents":self.agents, "coalitions":self.coalitions, "wars":self.wars, "gameover":self.gameover, "errors":self.errors, "actions":self.ai_actions}
        # fo "[GAME]:", 
        return pp.pprint(status)
    '''
    debugging method to play in cmd line
    ''' 
    def getFromJS(self):
        myinput = raw_input()
        return myinput

# if __name__ == "__main__":

#     app = Game()
#     app.start()
