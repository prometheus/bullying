from agent import Agent
import json
import random

class AI(Agent):

	def __init__(self, playerId):
		playerName = "AI-%d" %playerId
		Agent.__init__(self, playerId, playerName)

		self.status = "None"
		self.agents = None
		self.coalitions = None

	def updateWorld(self, agents, coalitions):
		self.agents = agents
		self.coalitions = coalitions


	def generateMove(self):
		action = "skip"
		coalition = "None"
		candidate = "None"

		if self.isLeader:
			decision = self.moveAsLeader()
		elif self.coalition:
			decision = self.moveAsMember()
		else:
			decision = self.moveAsFreeAgent()

		return decision


	def moveAsLeader(self):
		if self.isLeader:
			choice = 0
		
			freeAgent = []
			toChallenge = [] 

			for coalitionId in self.coalition.sent_invitations:
				print "coalition", coalitionId
				print self.coalitions.keys()

				if self.power > self.coalitions[str(coalitionId)]:
					return self.formatDecision("engage", coalitionId=str(coalitionId))

			for coalitionId in self.coalition.sent_invitations:

				if self.power < self.coalitions[str(coalitionId)]:
					return self.formatDecision("disengage", coalitionId=str(coalitionId))
			
			for coalition in self.coalitions.values():
				if self.power > coalition.power:
					return self.formatDecision("challenge", coalitionId=str(coalition.id))

			for agent in self.agents.values():
				if not agent.coalition and self.id not in agent.received_invitations:
					freeAgent.append(agent)

			if len(freeAgent) > 0:
				freeAgent.sort(key=lambda x: x.wealth, reverse=True)
				return self.formatDecision("invite", candidateId=str(freeAgent[0].id))

			if choice == 0:
				return self.formatDecision("skip")

	def moveAsMember(self):
		if self.coalition:
			# base choice = skip, leave
			# 25% chance of leaving
			choice = random.randint(0,4)

			if self.wealth * .75 > self.coalition.wealth:
				choice = random.randint(0,1)

			if choice == 0:
				return self.formatDecision("reflect")
			else:
				return self.formatDecision("skip", coalitionId=int(self.coalition.id))

	def moveAsFreeAgent(self):
		numInvites = self.received_invitations

		# base choice = skip and start coalition
		choices = 1

		if len(self.received_invitations) > 0:
			choices += len(self.received_invitations) * 3

		choice = random.randint(0,choices)

		# skip
		if choice == 0:
			return self.formatDecision("skip")

		elif choice == 1 or choice == 2:
			return self.formatDecision("start")

		elif choice > 1: 
			# 30% chance to deny invite
			invite_choice = random.randint(0, 4)
			candidateId = 0 if len(self.received_invitations) < 1 else random.randint(0, len(self.received_invitations)-1)
			print "invite",invite_choice
			print "coalition",candidateId
			if invite_choice == 0:
				return self.formatDecision("decline", candidateId=str(candidateId))
			else:
				return self.formatDecision("accept", candidateId=str(self.received_invitations[candidateId]))

	def formatDecision(self, decision, coalitionId="None", candidateId="None"):
		print "format", decision
		if decision == "skip": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "start": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "accept": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "decline": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "reflect": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "challenge": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "engage": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "disengage": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "invite": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

		if decision == "uninvite": 
			return json.dumps({"action":decision,"player": str(self.id), "coalition":coalitionId, "candidate":candidateId})

	