## UPDATED: 12/12 SAT

# GAME API

## Game Object 
```python
from app.game import Game
app = Game()
```

### Send server message 
```python
	message = "json message see below"
	app.receive(message)
	app.printGame() # prints status
```

### Message formats
You must send an "intial" message to start game with the number of players, 
then you can start sending "action" messages. 

## Initial Message
__First message with number of players must include key: "start"__

Game will check for key: "start" and parse this with unique init method
```json
{
	"start":"True",
	"players":"4",
	"names":["Bob","Jasmine","Dennis","Obama"]
}
```

__If length of names != number of players, game will sub AI__


Player names can be duplicated as player objects are identified
by their player ID. 

This is 0-based assigned in order of given name array.

```json
{
	"start":"True",
	"players":"4",
	"names":["Bob"]
}
```
__One user player "Bob" and three AI players__


## Send action
__All actions messages follow this JSON format__

```json
{
	"action":"user_action",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"player_id"
}
```
### start 
__player starts a coalition__
```json
{
	"action":"start",
	"player":"player_id",
	"coalition":"None",
	"candidate":"None"
}
```

### invite 
__player (leader) invites candidate to coalition__
```json
{
	"action":"invite",
	"player":"player_id",
	"coalition":"None",
	"candidate":"player_id"
}
```

### uninvite 
__player (leader) uninvites candidate to coalition__
```json
{
	"action":"uninvite",
	"player":"player_id",
	"coalition":"None",
	"candidate":"player_id"
}
```

### eject 
__player (leader) ejects candidate (member) from coalition__
```json
{
	"action":"eject",
	"player":"player_id",
	"coalition":"None",
	"candidate":"player_id"
}
```

### accept 
__player accepts existing invitation to coalition__
```json
{
	"action":"accept",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### refuse 
__player refuse existing invitation to coalition__
```json
{
	"action":"refuse",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### challenge 
__player (leader) sends challenge to coalition__
```json
{
	"action":"challenge",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### engage 
__player accepts war invitation from coalition__
```json
{
	"action":"engage",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### disengage 
__player cancels war invitation to coalition__
```json
{
	"action":"disengage",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

Note: player_id, coalition_id is an integer as string

See test.py for example

### Game status
## No AI players
__The game will return status updates via json after receiving server message__

An example with 2 AI's
```json
{"agents": [{"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 68.39084897080625, "Power": 341.95424485403123, "Coalition": 0}, "Id": "0", "Name": "Bob"},
            {"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 79.58149760628521, "Power": 397.90748803142606, "Coalition": 0}, "Id": 1, "Name": "AI-1"},
            {"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 116.02765342290853, "Power": 580.1382671145426, "Coalition": "None"}, "Id": 2, "Name": "AI-2"}],
 "clock": 3,
 "coalitions": [{"Status": {"Wealth": 421.53574246031644, "Power": 466.2983370022323, "sentInvitations": [], "Members": ["0", "1"], "receivedInvitations": [], "Leader": "0", "Size": 2}, "Id": 0}],
 "currentCoalitions": ["0"],
 "currentPlayers": [0, 1, 2],
 "gameover": "False",
 "log": "Bob (ID0) created a new coalition!, Bob (ID0) invited AI-1 (ID1) to their coalition (ID0), AI-1 (ID1) joined Bob (ID0)'s coalition (ID0) , AI-2 (ID2) skipped their turn",
 "playerTurn": 0,
 "wars": "None"}
```

An example after war occurs
```json
{"agents": [{"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 255.6875555338383, "Power": 1278.4377776691915, "Coalition": 1}, "Id": "0", "Name": "Bob"},
            {"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 154.4648271158455, "Power": 772.3241355792276, "Coalition": 1}, "Id": "1", "Name": "Jasmine"},
            {"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 181.8280723034502, "Power": 909.140361517251, "Coalition": 1}, "Id": "2", "Name": "Dennis"}],
 "clock": 36,
 "coalitions": [{"Status": {"Wealth": 591.980454953134, "Power": 2959.90227476567, "sentInvitations": [], "Members": ["2", "0", "1"], "receivedInvitations": [], "Leader": "2", "Size": 3}, "Id": 1}],
 "currentCoalitions": ["1"],
 "currentPlayers": [0, 1, 2],
 "gameover": "False",
 "log": "WAR!!! Dennis (ID2)'s coalition (ID1) defeated Obama (ID3)'s coalition (ID0)",
 "playerTurn": 0,
 "wars": {"Challenger": "0", "Loser": "0", "Challengee": "1", "Victor": "1"}}
```

An example skipping two full rounds
```json
{"agents": [{"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 415.9254916252891, "Power": 2079.6274581264456, "Coalition": 1}, "Id": "0", "Name": "Bob"},
            {"Status": {"sentInvitations": [], "receivedInvitations": [], "Wealth": 3127.9359544021345, "Power": 15639.679772010673, "Coalition": 1}, "Id": "2", "Name": "Dennis"}],
 "clock": 62,
 "coalitions": [{"Status": {"Wealth": 3543.8614460274234, "Power": 17719.307230137118, "sentInvitations": [], "Members": ["2", "0"], "receivedInvitations": [], "Leader": "2", "Size": 2}, "Id": 1}],
 "currentCoalitions": ["1"],
 "currentPlayers": [0, 2],
 "gameover": "True",
 "log": "Bob (ID0) skipped their turn, Gameover! No one moved for two full rounds!",
 "playerTurn": 0,
 "wars": "None"}
 ```
