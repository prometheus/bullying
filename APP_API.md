# GAME API

## Game Object 
```python
from app.game import Game
app = Game()
```

### Create game object and send server message
```python
	message = "json message see below"
	app.receive(message)
	app.printGame() # prints status
```

### Send initial message 
## Initialize Players
__First message with player number must include key: start__
```json
{
	"start":"True",
	"players":"4",
	"names":["Bob","Jasmine","Dennis","Obama"]
}
```

__Length of names must equal players__
Player names can be duplicated as player objects are identified
by their player ID. This is 0-based assigned in order of given name array.

## Send action
__All actions messages follow this JSON format__
__METHODS HAVE NOT BEEN FINISHED WITH TESTING___
```json
{
	"action":"user_action",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"player_id"
}
```
### start 
__player starts a coalition__
```json
{
	"action":"start",
	"player":"player_id",
	"coalition":"None",
	"candidate":"None"
}
```

### invite 
__player (leader) invites candidate to coalition__
```json
{
	"action":"invite",
	"player":"player_id",
	"coalition":"None",
	"candidate":"player_id"
}
```

### uninvite 
__player (leader) uninvites candidate to coalition__
```json
{
	"action":"uninvite",
	"player":"player_id",
	"coalition":"None",
	"candidate":"player_id"
}
```

### eject 
__player (leader) ejects candidate (member) from coalition__
```json
{
	"action":"eject",
	"player":"player_id",
	"coalition":"None",
	"candidate":"player_id"
}
```

### accept 
__player accepts existing invitation to coalition__
```json
{
	"action":"accept",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### refuse 
__player refuse existing invitation to coalition__
```json
{
	"action":"refuse",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### challenge 
__player (leader) sends challenge to coalition__
```json
{
	"action":"challenge",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### engage 
__player accepts war invitation from coalition__
```json
{
	"action":"engage",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

### disengage 
__player declines war invitation from coalition__
```json
{
	"action":"disengage",
	"player":"player_id",
	"coalition":"coalition_id",
	"candidate":"None"
}
```

Note: player_id, coalition_id is an integer as string
See test.py for example
